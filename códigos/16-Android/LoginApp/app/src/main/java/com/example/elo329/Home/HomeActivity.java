package com.example.elo329.Home;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.elo329.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
