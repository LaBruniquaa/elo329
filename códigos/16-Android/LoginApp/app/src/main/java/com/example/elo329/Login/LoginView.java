package com.example.elo329.Login;

public interface LoginView {
    void onLoginSucceed();
    void onLoginFailed();
}
